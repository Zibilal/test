# Reviews

**Ceritakan apa itu OOP, object oriented programming**

*Teknik penulisan code yang mengikuti sifat objek itu sendiri dimana objek-objek tersebut
 dapat saling berhubungan/berkoneksi. Fungsi penting dari oop itu sendiri adalah agar
 mudah dimaintenance apalagi saat system sudah besar dengan teknik penulisan yang lebih
 rapi/mudah dibaca dibandingkan teknik terstruktur yang lebih berantakan/sulit dibaca ketika
 system sudah besar, mengurangi repetitive code seperti contoh objek kendaraan terdiri dari
 objek mobil dan objek motor dimana objek mobil dan motor itu mempunyai turuan sifat asli
 dari objek kendaraan dan objek mobil sendiri mempunyai attribute dan fungsi yang lebih
 spesifikasi lagi ditambah/extend attribute dan fungsi objek kendaraan itu sendiri sebagai
 sifat super aslinya begitu juga dengan motor.*
 
 Contoh Gambaran :

![gambar-1](gambar-1.png)

**Remark**

Jawaban terlalu dangkal. OOP mempunyai beberapa komponen penting. Hal ini tidak diterangkan.

**Score**

**2/4**

---

**Di java 8 ada dua komponen untuk implementasi abstraksi lebih baik, yaitu stream dan lambda. 
Ceritakan apa itu. Pada saat apakah kita membutuhkan stream atau lambda.**

*Stream utk collect array list tanpa perulangan for
 Lambda utk menyederhanakan code yang interfacenya hanya memiliki satu fungsi seperti
 run thread.*

**Remark**

Jawaban tidak menjawab pertanyaan.

**Score**

**0/4**

---

**Apa perbedaan antara interface, abstract kelas.**

*Interface : turunan tanpa tubuh > implement
 Abstract : turunan dengan tubuh > extend*
 
 **Remark**
 
 Jawaban terlalu dangkal. Tidak menjawab prinsip mengapa dibutukan dua konsep diimplement dijava.
 
 **Score**
 
 **1/4**

___

**Apa itu functional interface, dibutuhkan pada saat apakah?**

*Karna inherit extend java hanya satu maka jika mau turunan lebih maka menggunakan
 interface praktisnya utk penamaan turunan spesifik lainnya agar mudah dibaca dan juga utk
 polymorphism override, fungsi yang sama tapi berbeda perlakuan, seperti fungsi hitung luas
 utk segitiga,persegi berbeda cara.*
 
**Remark**

Jawaban tidak menjawab pertanyaan.

**Score**

**0/4**

---

**Terangkan apa itu SOLID principle. Apakah berguna?**

*Menambah boleh tapi jangan mengubah. Kalau ada yg diubah maka riskan berpengaruh
 pada turunannya.*
 
**Remark**

Jawaban dangkal

**Score**

**1/4**

---

**Jika saya mempunyai integer : 1234567 , buat lah fungsi yang keluarannya 7654321 tanpa
  menggunakan library, atau string prosesing.**
  
**Remark**

Sesuai

**Score**

**2/2**

---

**Saya memiliki matrix sebagai berikut:**

![gambar-2](gambar-2.png)
  
 **Buat matrix class yang mempunya behavior untuk perkalian matrix. Coba untuk
  menyelesaikan perkalian matrix diatas.**
  
 **Remark**
 
 Jawaban kurang lengkap. Behavior biasanya dihubunkan dengan method dalam oop.
 
 **Score**
 
 **1/2**
 
 ---
 
**Apa itu unit testing. Bedakah dengan integration testing? Apa bedanya?**
 
 *Unit testing : detail test code utk melihat ada bug dan membuat code menjadi reusable
 Integration testing : dilakukan setelah unit testing, test kesesuaian, performance, kualitas
 system*
 
 **Remark**
 
 Jawaban tidak tepat
 
 **Score**
 
 **0/4**
 
 ---
 
 **Terangkan apa itu TDD.**
 
 *Test kehandalan system apakah system sudah memenuhi persyaratan.*
 
 **Remark**
 
 Jawaban tidak tepat
 
 **Score**
 
 **0/4**
 
 ___
 ___
 
 **Total Score**
 
 (2 + 0 + 1 + 0 + 1 + 2 + 1 + 0 + 0) / (4 + 4 + 4 + 4 + 4 + 2 + 2 + 4 + 4) =
 7 / 30 = 0.23 * 100% = 23%
 
 **Nilai yang dicari**
 
 **80%**