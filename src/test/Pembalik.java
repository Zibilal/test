package test;

public class Pembalik {
	
	static int balik(int angka) {
		int n = angka;
		int nb = 0;
		nb = n % 10;
		while(n != 1) {
			nb = nb * 10 + (n / 10) % 10;
			n = n  / 10;
		}
			
		return nb;
	}
	
	public static void main(String[] args) {
		System.out.println(balik(1234567));
	}
}
