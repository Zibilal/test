package test;

public class Matrix {
	public static void main(String[] args) {
		int m1[][] = {{1,2,3},{4,5,6},{7,8,9}};
		int m2[][] = {{10},{11},{12}};
		
		for(int i=0;i<m1.length;i++) {
			for(int l=0;l<m2[0].length;l++) {
				int n=0;
				for(int j=0;j<m1[i].length;j++) {
					n+=m1[i][j]*m2[j][l];
				}
				System.out.print(n+"\t");
			}
			System.out.println();
		}
	}
}
